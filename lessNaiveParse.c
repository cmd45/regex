#include<stdio.h>
#include<stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <pthread.h>
#include "someRegexTables.h"
typedef struct{
	int* stateArrays;
	int startingIndex;
	int finishIndex;
	char* fullString;
}parser_t;
int numStates = 4;
int length = 36;
int numThreads = 9;
void* parse(void* state){
        parser_t* parser_state = (parser_t*)state;
        int i,k = 0;
        int start = parser_state->startingIndex;
        int finish = parser_state->finishIndex;
        char* totalString = parser_state->fullString;
        int* stateArrays = parser_state->stateArrays;
	int fullLength = finish - start;
	int addressNumStates = numStates;
     //   int* statesReached =
	for(k = 0; k < numStates; k++ ){
	//	printf("k: %d, start: %d, currState =%d\n", k,start,DeltaTable_400[totalString[start]][k]);
	//	printf("currentK: %d startArrays: %d \n",k, stateArrays[0]);
		stateArrays[addressNumStates * start + k] = DeltaTable_400[totalString[start]][k];
		printf("thread: %d is at character %c at starting state %d \n",start, totalString[start], (i+1) * addressNumStates + k );
	}
        for(i = start; i < finish; i++){
                char currChar = totalString[i];
                for(k = 0; k < numStates;k++){
			printf("thread: %d -> %d is at character %c at starting state %d, with k: %d \n",start, finish, currChar, (i+1) * addressNumStates + k,k );
                        stateArrays[(i+1)*addressNumStates + k] = DeltaTable_400[currChar][stateArrays[(i)*addressNumStates + k]];
        //                if(statesReached[stateArrays[k][i]] >= 0)
                }
        }
}


int main(){
	char parsingString[] = "1234567890abcdefghijklmnopqrstuvwxyz";
	int state=0;
	int i=0;
//	int numThreads = 9;
//	int numStates = 4;
	pthread_attr_t attr;
	int parseLengthForEachThread = length/numThreads;
	int stateArrays [numStates*length];
	memset(stateArrays, -1, sizeof(int)*numStates*length);
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_t* threads = (pthread_t*)malloc(sizeof(pthread_t)*numThreads);
	for(i = 0 ; i < numThreads ; i++){
		parser_t* stateData = (parser_t*)malloc(sizeof(parser_t));
		threads[i] = 0;
		stateData -> startingIndex = length/numThreads * i;
		stateData -> finishIndex = length/numThreads *(i+1);
		stateData -> fullString = parsingString;
		stateData -> stateArrays = stateArrays;
		printf("thread %d created!! start: %d, finish: ",i, stateData -> startingIndex);
		int rc = pthread_create(&threads[i],&attr,parse , (void*)stateData);
		if(rc){
			printf("ERROR: rc from pthread_create --> %d\n", rc);
			exit(-1);
		}	   
	}	
	pthread_attr_destroy(&attr);
	for(i = 0; i < numThreads; i++){
	int	rc = pthread_join(threads[i], NULL);
		if(rc){
			printf("ERROR: %d\n", rc);
		}
	}
printf("program runout\n");
return 0;
}

